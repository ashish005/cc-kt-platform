import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AccountRoutes } from './account.routes';
import { LoginComponent } from './login/login.component';
import {RegisterComponent} from './register/register.component';

@NgModule({
  imports: [
    // Modules
    CommonModule,
    FormsModule,
    RouterModule.forChild(AccountRoutes)
  ],

  declarations: [
    // Components & Directives
    LoginComponent, RegisterComponent
  ],

  providers: [
    // Services
  ],

  exports: [

  ]
})

export class AccountModule { }
