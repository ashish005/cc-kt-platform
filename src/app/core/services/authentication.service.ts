import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Rx';

import { Configuration } from '../../shared/configuration/app.configuration';
import {CurrentUserService} from './current-user.service';

@Injectable()
export class AuthenticationService {

  public redirectUrl: string;

  constructor(private http: HttpClient,
              private currentUserService: CurrentUserService,
              private router: Router,
              private configuration: Configuration) {

  }

  loginUser(username: string, password: string): Observable<any> {
    const clientId = 'client_id=' + 'client id';
    const usernameForBody = 'username=' + username;
    const passwordForBody = 'password=' + password;

    const body = clientId.concat('&', usernameForBody, '&', passwordForBody);

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    return Observable.create((observer: Observer<any>) => {
      this.http.post<any>(this.configuration.server + 'connect/token', body, options)
        .subscribe((tokenData: any) => {
            this.currentUserService.token = tokenData.access_token;
            this.currentUserService.username = username;
            observer.next(tokenData);
          }, (error) => observer.error(error),
          () => observer.complete());
    });
  }

  logoutUser() {
    this.currentUserService.token = null;
    this.currentUserService.username = null;
    this.router.navigate(['/home']);
  }
}