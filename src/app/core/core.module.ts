import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import {AuthenticationService} from './services/authentication.service';
import {StorageService} from './services/storage.service';
import {HttpConfigurableInterceptor, HttpWrapperService} from './services/http-wrapper.service';
import {CurrentUserService} from './services/current-user.service';

@NgModule({
  imports: [CommonModule],
  exports: [],
  declarations: [],
  providers: [
    // see below
  ],
})

export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AuthenticationService,
        HttpWrapperService,
        StorageService,
        CurrentUserService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: HttpConfigurableInterceptor,
          multi: true,
        }]
    };
  }
}