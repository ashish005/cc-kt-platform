import {AppLayout} from './master-layout/app.layout';
import {HomeComponent} from './home/home.component';

export {AppLayout} from './master-layout/app.layout';
export {HomeComponent} from './home/home.component';

export const PLATFORM_COMPONENTS = [ AppLayout, HomeComponent];
