import { NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlatformRouting} from './platform.routes';
import {PLATFORM_COMPONENTS} from './index';

@NgModule({
  imports: [
    CommonModule,
    PlatformRouting
  ],
  entryComponents: [ ],
  providers: [  ],
  declarations: [ PLATFORM_COMPONENTS ]
})

export class PlatformModule implements OnInit {
  constructor() {}
  ngOnInit(){}
}