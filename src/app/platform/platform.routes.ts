import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {AppLayout, HomeComponent} from './index';

export const PlatformRoutes: Routes = [
  {
    path: '',
    component: AppLayout,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent }
    ]
  },
];

export const PlatformRouting: ModuleWithProviders = RouterModule.forChild(PlatformRoutes);