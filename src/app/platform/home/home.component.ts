import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  templateUrl: './home.html'
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  constructor() {}
  ngOnInit() {}
  ngOnDestroy() {}
  ngAfterViewInit(): void {
  }
}