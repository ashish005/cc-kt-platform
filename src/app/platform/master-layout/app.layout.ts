import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  templateUrl: './app.layout.html'
})
export class AppLayout implements OnInit, AfterViewInit, OnDestroy {
  constructor() {}
  ngOnInit() {}
  ngOnDestroy() {}
  ngAfterViewInit(): void {
  }
}
