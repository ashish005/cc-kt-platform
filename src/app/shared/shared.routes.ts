import { RouterModule, Routes } from '@angular/router';
import {NotFoundComponent, LockScreenComponent, ErrorComponent} from './components';
import {ModuleWithProviders} from '@angular/core';

export const SharedRoutes: Routes = [
  { path: '505', component: NotFoundComponent },
  { path: '400', component: ErrorComponent },
  { path: 'lock-screen', component: LockScreenComponent },
];

export const SharedRouting: ModuleWithProviders = RouterModule.forRoot(SharedRoutes);