import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {AuthGuard} from './guards/authentication.guard';
import {SHARED_COMPONENTS} from './components';
import {SharedRouting} from './shared.routes';
import {Configuration} from './configuration/app.configuration';


@NgModule({
  imports: [
    // Modules
    CommonModule,
    SharedRouting
  ],
  declarations: [
    // Components & directives
    SHARED_COMPONENTS
  ],
  providers: [
    // Services
    Configuration,
    AuthGuard
  ],
  exports: []
})

export class SharedModule {
  constructor() {
  }
}
