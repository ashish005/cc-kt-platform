import {NotFoundComponent} from './not-found/not-found.component';
import {LockScreenComponent} from './lock-screen/lock-screen.component';
import {ErrorComponent} from './error/error.component';

export {NotFoundComponent} from './not-found/not-found.component';
export {LockScreenComponent} from './lock-screen/lock-screen.component';
export {ErrorComponent} from './error/error.component';

export const SHARED_COMPONENTS = [ NotFoundComponent, LockScreenComponent, ErrorComponent];