import { Injectable } from '@angular/core';
import { ToasterConfig } from 'angular2-toaster/angular2-toaster';

import { environment } from '../../../environments/environment';

@Injectable()
export class Configuration {
  server = environment.production ? 'server url' : 'dev url';
}