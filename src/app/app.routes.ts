import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AuthGuard} from './shared/guards/authentication.guard';

export const AppRoutes: Routes = [
  { path: 'account', loadChildren: './account/account.module#AccountModule' },
  { path: '', loadChildren: './platform/platform.module#PlatformModule', canLoad: [AuthGuard] },
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(AppRoutes, { useHash: true, preloadingStrategy: PreloadAllModules });