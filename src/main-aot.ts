import './polyfills';
import './vendor';

import { enableProdMode } from '@angular/core';
import { platformBrowser } from '@angular/platform-browser';
import {AppModuleNgFactory} from '../compiled/aot/src/app/app.module.ngfactory';
// Entry point for AoT compilation.

enableProdMode();

platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);